import abc
from itertools import chain

from django.db.models.fields.related import ForeignRelatedObjectsDescriptor, ReverseSingleRelatedObjectDescriptor


class LookupMixin(object):

    __metaclass__ = abc.ABCMeta

    request = None
    kwargs = dict()

    @abc.abstractmethod
    def get_object(self):
        return super(LookupMixin, self).get_object()

    def get_queryset(self):
        queryset = super(LookupMixin, self).get_queryset()
        return self.lookup(queryset, self.request, self.kwargs)

    @classmethod
    @abc.abstractmethod
    def lookup(cls, queryset, request, kwargs):
        """
        This is a main method of LookupMixin which should be overridden with every implementation.
        :param queryset: original view set queryset
        :param request: view set request
        :param kwargs: view kwargs
        :return: lookup queryset
        """
        pass


class ParentLookupMixin(LookupMixin):
    """
    This lookup mixin is for receiving an object / list of objects which are related to the
    parent instance by ForeignKey. We assume that parent will be explicitly defined by it's ID
    as an integer in the middle of endpoint and can be accessed via the parent_url_kwarg in kwargs.
    """
    parent = None
    parent_url_kwarg = None
    
    def get_object(self):
        return super(ParentLookupMixin, self).get_object()

    @classmethod
    def lookup(cls, queryset, request, kwargs):
        assert cls.parent_url_kwarg is not None, "Please, define parent_url_kwarg before you can use ParentLookupMixin"
        assert cls.parent_url_kwarg in kwargs.keys(), "Be sure to pass your parent's ID as a '%s' value through " \
                                                      "the URL endpoint" % cls.parent_url_kwarg

        parent = cls.get_parent_instance(kwargs)
        return cls._filter_by_parent(queryset, parent)

    @property
    def parent_id(self):
        return self.kwargs.get(self.parent_url_kwarg)

    @classmethod
    def get_parent_instance(cls, kwargs):
        """
        Get the parent instance by it's id taken from kwargs
        :param kwargs: url kwargs
        :return: parent instance
        """
        try:
            parent = cls.parent.objects.get(id=kwargs.get(cls.parent_url_kwarg))
        except Exception:
            parent = None
        return parent

    @classmethod
    def _get_parent_field(cls, model, parent):
        """
        Find a field name of a reverse single related object by the 'parent' instance reference
        :param model: django model
        :param parent: parent instance to be used as a reference to find a field name
        :return: string parent field name
        """
        if hasattr(parent, 'id'):
            parent = type(parent)

        single_related_objects = filter(lambda x: isinstance(x, ReverseSingleRelatedObjectDescriptor),
                                        model.__dict__.values())
        try:
            parent_related_field = filter(lambda x: x.field.related_model == parent, single_related_objects)[0]
        except IndexError:
            parent_related_field = None
        else:
            parent_related_field = parent_related_field.field.name
        return parent_related_field

    @classmethod
    def _filter_by_parent(cls, queryset, parent):
        """
        Filter queryset and return only records created by user.
        :param queryset: queryset to be filtered
        :param parent: parent instance
        :return: filtered queryset
        """
        parent_field = cls._get_parent_field(queryset.model, parent)
        return queryset.filter(**{'%s__id' % parent_field: parent.id})


class UserOwnLookupMixin(LookupMixin):
    """
    This lookup mixin may be used with any view set to assemble only those instances which
    are related to user (in other words, which are belongs to user).
    Use user_field to determine the field which contain a link to the UserProfile model.
    """

    user_field = 'user'

    def get_object(self):
        obj = super(UserOwnLookupMixin, self).get_object()
        user = getattr(getattr(self.request, 'user', None), 'userprofile', None)
        if getattr(obj, self.user_field) == user:
            return obj
        else:
            return None

    @classmethod
    def lookup(cls, queryset, request, kwargs):
        user = getattr(request, 'user')
        return cls._filter_by_user(queryset, user)

    @classmethod
    def _filter_by_user(cls, queryset, user):
        """
        Filter queryset and return only records created by user.
        :param queryset: queryset to be filtered
        :param user: UserProfile
        :return: filtered queryset
        """
        return queryset.filter(**{cls.user_field: user})


class UserOwnChildrenLookup(UserOwnLookupMixin):
    """
    This lookup mixin will fetch only children instances he belongs to. To describe this relation
    we need two fields:
    - children instance related_name
    - user field of children instance
    """

    related_name = 'children'
    user_field = 'user'

    def get_object(self):
        obj = super(UserOwnLookupMixin, self).get_object()
        user = getattr(self.request, 'user')
        children_instances = getattr(user, self.related_name, None)
        if not children_instances:
            return None

        if children_instances.filter(**{self.user_field: user}).count() > 0:
            return obj
        else:
            return None

    @classmethod
    def _filter_by_user(cls, queryset, user):
        """
        Filter queryset and return only records which has children owned by user.
        :param queryset: queryset to be filtered
        :param user: UserProfile
        :return: filtered queryset
        """
        filter_field = '%s__%s' % (cls.related_name, cls.user_field)
        return queryset.filter(**{filter_field: user})


class LookupChain(LookupMixin):
    """
    This mixin is a one-directional (1 level) chain of responsibility which is designed to
    combine multiple lookups into one single queryset. The way how two queryset will be combined
    is just by merging them together. You may think of OR operator handling this process.
    """

    lookup_mixins = []

    @classmethod
    def lookup(cls, queryset, request, kwargs):
        queryset_list = list()
        for successor in cls.lookup_mixins:
            if hasattr(successor, 'lookup'):
                queryset_list.append(successor.lookup(queryset, request, kwargs))
        merged_results = list(chain(*queryset_list))
        return queryset.model.objects.filter(id__in=[r.id for r in merged_results])

    def get_object(self):
        obj = None
        for lookup in self.lookup_mixins:
            if hasattr(lookup, 'get_object'):
                obj = lookup.get_object()
                if obj is None:
                    break
        return obj
